#![crate_type = "staticlib"]
#![no_std]
#![feature(lang_items)]

#[no_mangle]
pub extern fn get_message_from_rust() -> *const u8 {
    let foo = b"Hello ... I am a string literal from Rust\0";
    foo.as_ptr()
}

// This declares the Contiki function that has this signature in C
//      void leds_toggle(unsigned char ledv);
extern {
    fn leds_toggle(ledv: u8) -> ();
}

#[no_mangle]
pub extern fn do_something_rusty() -> () {
    // Invoke Contiki to toggle led
    let led1 :u8 = 1;
    unsafe {
        leds_toggle(led1);
    }
}

#[no_mangle]
pub extern fn double_input(input: i32) -> i32 {
    input * 2
}

#[lang = "panic_fmt"]
#[no_mangle]
pub extern fn panic_fmt() -> ! {loop{}}
